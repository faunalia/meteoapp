# SouthTyrol Meteo App

The idea behind this project is to be able to analyze some specific data of
South Tyrol. Thanks to the [local
government](http://meteo.provincia.bz.it/download-dati.asp) a lot of data are
available for many different locations and sensors.

To be able to quickly analyze the data, this are downloaded from the official
[Website](http://daten.buergernetz.bz.it/it/dataset/misure-meteo-e-idrografiche)

## Requirements

All the scripts are written in python3. Plots are created using
[Dash](https://dash.plot.ly), a [Plotly](https://plot.ly) component to create
remote plots frameworks.

## Installation

If you want to deploy the current repository, I strongly suggest to create a
python virtual environment and use the `REQUIREMENTS.txt` file to install all
the dependencies.

Follow the instruction of your local OS to install a [python3 virtual
environment](https://virtualenv.pypa.io/en/stable/).

After the installation, create a **virtualenv** (following example is Linux based):

```
cd my_location
python3 -m venv my_virtual_env_name
```

and source it:

```
source my_virtual_env_name/bin/activate
```

Now you can install all the required dependencies that are stored in the
`REQUIREMENTS.txt` file:

```
pip install -r REQUIREMENTS.txt
```

Finally start the Dash-Flask app with:

```
python __init__locale.py
```

and visit `http://127.0.0.1:8050/meteo-app/` on your browser to see the live
example.

The raw data are not taken *live* from the database of the local county. All the
data have been taken and wrapped into a PostgreSQL database and are continuosly
updated with a `cronjob` every day, therefore, a specific **readonly** user has
been created on the server.
