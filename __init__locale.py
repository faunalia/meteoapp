import flask
import dash
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State

import plotly.graph_objs as go
import plotly.express as px

import psycopg2
from collections import defaultdict
import requests
import json
import datetime
from datetime import datetime as dt
import pandas as pd
import random



server = flask.Flask(__name__)

#@server.route('/')
#def index():
#    return 'Hello Flask app'


### Step 1.1: outside the APP, fetch the station and sensor lists and build
### dictionaries to use later

# get URL and all the sensor object
sensors_url = 'http://daten.buergernetz.bz.it/services/meteo/v1/sensors?'
sensors_stations = requests.get(sensors_url).text
sensors_stations = json.loads(sensors_stations)

# get URL and all the stations object
stations_url = 'http://daten.buergernetz.bz.it/services/meteo/v1/stations'
stations = requests.get(stations_url).text
stations = json.loads(stations)

# define defaultdict with key = SCODE and value = dict of sensors codes and names
# e.g. d = {'S1': {'N': 'Precipitazione', 'WT': 'Temperatura Acqua'}, 'S2': {'N': 'Precipitazione'}}
stat_sens = defaultdict(dict)

# fill the dict of dict
for i in sensors_stations:
   stat_sens[i['SCODE']][i['TYPE']] = {'it': i['DESC_I'], 'de': i['DESC_D'], 'unit' : i['UNIT']}
   # stat_sens[i['SCODE']][i['TYPE']] = i['DESC_I']


# d = {'C1': {'it': 'Brunico', 'de': 'Bruneck'}}
station_dict = defaultdict(dict)

for i in stations['features']:
    station_dict[i['properties']['SCODE']] = {
        'it': i['properties']['NAME_I'],
        'de': i['properties']['NAME_D'],
        'en': i['properties']['NAME_E'],
        }

# load default template and bootstrap GRID
external_stylesheets = [
    'https://codepen.io/chriddyp/pen/bWLwgP.css',
    dbc.themes.GRID,
]

r = lambda: random.randint(0,255)

# create the Dash app at a specific route
app = dash.Dash(
    __name__,
    server=server,
    routes_pathname_prefix='/meteo-app/',
    external_stylesheets=external_stylesheets
)

# create the Dash App Layout
app.layout = html.Div(
    [
        # header
        html.Div(
            [
                # title and subtitle
                html.Div(
                    [
                        html.A(
                            html.H4(
                                "SouthTyrol Meteo App",
                            ),
                            href="https://www.faunalia.eu",
                            target="_blank",
                            className="app__header__title"
                        ),
                        html.P(
                            "This APP creates Meteo plots of South Tyrol",
                            className="app__header__title--grey"
                        )
                    ],
                className="app__header__desc",
                ),

                # logo
                html.Div(
                    [
                        html.A(
                            html.Img(
                            src=app.get_asset_url("logo_faunalia.png"),
                            className="app__menu__img"
                            ),
                            href="http://www.faunalia.eu",
                            target="_blank"
                        )
                    ],
                    className="app__header__logo",
                ),
            ],
            className="app__header",
        # end of header
        ),

        # entire body that contains all the sandbox (combobox, plots, etc)
        html.Div(
            [

                dbc.Container(
                    [
                        dbc.Row(
                            [
                                # first column
                                dbc.Col(
                                    [
                                        html.Label(
                                            "Select the station"
                                        ),
                                        dcc.Dropdown(
                                            id="stations",
                                            options=[
                                                {"label": v['it'].title(), "value": k} for k, v in station_dict.items()
                                            ],
                                            className="auto__p__combo"
                                        )
                                    ]
                                ), # end of first column

                                # second column
                                dbc.Col(
                                    [
                                        html.Label(
                                            "Choose the sensor"
                                        ),
                                        dcc.Dropdown(
                                            id="sensors",
                                            className="auto__p__combo"
                                        )
                                    ]
                                ), # end of second column

                                # third column
                                dbc.Col(
                                    [
                                        html.Label(
                                            "Select the date range"
                                        ),
                                        dcc.DatePickerRange(
                                            id="date-picker-select",
                                            min_date_allowed=dt(2014, 8, 1),
                                            max_date_allowed=dt.today(),
                                            initial_visible_month=dt.today(),
                                            # initial_visible_month=dt(2019, 8, 1),
                                            className="auto__p__combo"
                                        )
                                    ]
                                ), # end of third column

                                dbc.Col(
                                    [
                                        html.Button(
                                            "Aggiungi Stazione",
                                            id="add-button",
                                            n_clicks_timestamp=0
                                        )
                                    ]
                                ),

                                dbc.Col(
                                    [
                                        html.Button(
                                            "Reset",
                                            id="reset-button",
                                            n_clicks_timestamp=0
                                        )
                                    ]
                                ),
                            ]
                        ) # end of row
                    ]
                ), # end of container

                # plots Div
                html.Div(
                    [
                        # left side (main) plot
                        html.Div(
                            [
                                dcc.Graph(
                                    id="main-plot",
                                    # figure=go.Figure()
                                )
                            ],
                        className="sensor__plots"
                        ),

                        # right side plot
                        html.Div(
                            [
                                dcc.Graph(
                                id="secondary-plot",
                                figure=go.Figure()
                            )
                        ],
                        className="sensor__plots"
                        )
                    ],
                    className="app__content",

                )
            ],
        ),

        # footer
        html.Div(
            [
                html.Div(
                    [
                        html.A(
                            html.P(
                                "Original Data taken from the Official South Tyrol Open Data Portal",
                            ),
                            href="http://meteo.provincia.bz.it/",
                            target="_blank",
                            className="app__comment"
                        ),
                    ]
                )
            ]
        )

    ],
    className="app__container",
)


# callback to update the sensor list checkbox depending on the station chosen
@app.callback(
    Output('sensors', 'options'),
    [Input('stations', 'value')])
def set_sensors(selected_station):
    return [{'label': v['it'], 'value': k} for k, v in stat_sens[selected_station].items()]



# callback to update the main plot
# the only output is the main plot Figure object while different inputs have to
# be specified. If not all the inputs are specified the function returns an empty plot
@app.callback(
    [Output('main-plot', 'figure'),
    Output('secondary-plot', 'figure')],
    [Input('add-button', 'n_clicks_timestamp'),
    Input('reset-button', 'n_clicks_timestamp')],
    [State('stations', 'value'),
    State('sensors', 'value'),
    State('date-picker-select', 'start_date'),
    State('date-picker-select', 'end_date'),
    State('main-plot', 'figure'),
    State('secondary-plot', 'figure')]
)
def update_main_plot(n_clicks, reset_clicks, station, sensor, start_date, end_date, plot, splot):

    if int(n_clicks) > int(reset_clicks):

        # only if all the input have be defined, call the function to create the Figure
        if station and sensor and start_date and end_date:

            figures = create_plots(station, sensor, start_date, end_date, n_clicks, plot, splot)
            fig1 = figures['p1']
            fig2 = figures['p2']

        # if some inputs are missing, create a valid but empty Figure
        else:
            fig1 = go.Figure()
            fig2 = go.Figure()

        # return the Figure object and show it in the plot
        return fig1, fig2

    else:

        fig1 = go.Figure()
        fig2 = go.Figure()

        # return the Figure object and show it in the plot
        return fig1, fig2


def create_plots(station, sensor, start_date, end_date, n_clicks, plot, splot):
    '''
    Custom function to be called within the main plot callback

    All the input are taken from the callback main function (input and state).

    The only Input is n_clicks that verifies if the click button has been clicked.
    All the others are States in order to prevent an automatic refresh

    param:
        station (str): station of the combobox
        sensor (str): sensor of the combobox
        start_date (date): initial date of the daterange
        end_date (date): end date of the daterange
        n_clicks (int): number of clicks made on the button
        plot (figure): main plot figure. Passing it to the function allows to
        have the figure as an usable object
        splot (figure): secondary plot figure. Passing it to the function allows
        to have the figure as an usable object

    Returns: dictionary with the 2 Figure objects suitable for the plots

    '''

    ## First Main Plot

    # if the sensor is rain make an hourly SUM of the values
    if sensor == 'N':

        query_main_plot = '''
            SELECT
                DATE_TRUNC('hour', date) AS dateh,
                SUM({sensor}) AS rain
            FROM _{station}
                WHERE
                    date BETWEEN '{start_date}' AND '{end_date}'
                AND
                    {sensor} IS NOT NULL
                GROUP BY dateh
                ORDER BY dateh DESC
        '''.format(
            sensor=sensor.replace('.', '_').lower(),
            station=station.lower(),
            start_date=start_date,
            end_date=end_date
        )

        with psycopg2.connect(service='meteo') as mycon:

            df = pd.read_sql_query(query_main_plot, mycon)

        # if the first location is chosen and no plots have been created yet
        if not plot['data']:

            fig1 = go.Figure()
            fig1.add_trace(
                go.Bar(
                    x=df['dateh'],
                    y=df['rain'],
                    name='{}'.format(station_dict[station]['it']),
                    showlegend=True,
                    marker_color='#%02X%02X%02X' % (r(),r(),r())
                )
            )

        # if there are already plots, then append other location plots
        else:

            fig1 = go.Figure()

            for i in plot['data']:
                fig1.add_traces(
                    go.Bar(
                        x=i['x'],
                        y=i['y'],
                        name=i['name'],
                        marker_color=i['marker']['color']
                    )
                )

            fig1.add_trace(
                go.Bar(
                    x=df['dateh'],
                    y=df['rain'],
                    name='{}'.format(station_dict[station]['it']),
                    marker_color='#%02X%02X%02X' % (r(),r(),r()),
                    showlegend=True
                )
            )


    # other sensors
    else:

        # wind direction
        if sensor == 'WR':

            query_main_plot = '''
                SELECT
                    AVG(t) AS wg,
                    wr
                FROM
                    (SELECT
                    --DATE_TRUNC('hour', date) AS dateh,
                    CASE WHEN AVG({sensor}) BETWEEN 45 AND 135 THEN 'E'
                    WHEN AVG({sensor}) BETWEEN 135 AND 225 THEN 'S'
                    WHEN AVG({sensor}) BETWEEN 225 AND 315 THEN 'W'
                    ELSE 'N'
                    END
                    AS wr,
                    AVG(wg) AS t FROM _{station}
                    WHERE date BETWEEN '{start_date}' AND '{end_date}'
                    AND
                    {sensor} IS NOT NULL
                    GROUP BY wr) p
                    GROUP BY wr
                    --ORDER BY dateh DESC
            '''.format(
                sensor=sensor.replace('.', '_').lower(),
                station=station.lower(),
                start_date=start_date,
                end_date=end_date
            )

            # fetch the data from the query
            # try - except block needed when changing station that does not have sensor
            with psycopg2.connect(service='meteo') as mycon:

                df = pd.read_sql_query(query_main_plot, mycon)

            # first plot
            fig1 = px.bar_polar(
                df,
                r="wg",
                theta="wr",
            )

        else:

            # all other sensors

            query_main_plot = '''
                SELECT
                    DATE_TRUNC('hour', date) AS dateh,
                    AVG({sensor}) AS {sensor}
                FROM _{station}
                    WHERE
                        date BETWEEN '{start_date}' AND '{end_date}'
                    AND
                        {sensor} IS NOT NULL
                    GROUP BY dateh
                    ORDER BY dateh DESC
            '''.format(
                sensor=sensor.replace('.', '_').lower(),
                station=station.lower(),
                start_date=start_date,
                end_date=end_date
            )


            with psycopg2.connect(service='meteo') as mycon:

                df = pd.read_sql_query(query_main_plot, mycon)

            if not plot['data']:

                fig1 = go.Figure()
                fig1.add_trace(
                    go.Scatter(
                        x=df['dateh'],
                        y=df['{}'.format(sensor.replace('.', '_').lower())],
                        name='{}'.format(station_dict[station]['it']),
                        mode='lines',
                        showlegend=True,
                        marker_color='#%02X%02X%02X' % (r(),r(),r())
                    )
                )

            # if there are already plots, then append other location plots
            else:

                fig1 = go.Figure()

                for i in plot['data']:
                    fig1.add_traces(
                        go.Scatter(
                            x=i['x'],
                            y=i['y'],
                            name=i['name'],
                            marker_color=i['marker']['color']
                        )
                    )

                fig1.add_trace(
                    go.Scatter(
                        x=df['dateh'],
                        y=df['{}'.format(sensor.replace('.', '_').lower())],
                        name='{}'.format(station_dict[station]['it']),
                        mode='lines',
                        showlegend=True,
                        marker_color='#%02X%02X%02X' % (r(),r(),r())
                    )
                )


    ## Second Plot

    # try - except mandatory if the second station chosen does not have the N sensor
    try:

        query_second_plot = '''
            SELECT
                EXTRACT(month FROM day_date) AS mese,
                DATE_TRUNC('month', day_date) AS month_date,
                COUNT(n) FILTER (WHERE n = 0) AS no_rain,
                COUNT(n) FILTER (WHERE n != 0) AS rain,
                COUNT(n) AS all_values
                FROM
                    (SELECT
                    DATE_TRUNC('day', date) AS day_date,
                    SUM(n) AS n FROM _{station}
                    WHERE date BETWEEN '{start_date}' AND '{end_date}'
                    GROUP BY day_date
                    ORDER BY day_date desc) p
                GROUP BY mese, month_date
                ORDER BY month_date DESC
        '''.format(
            station=station.lower(),
            start_date=start_date,
            end_date=end_date
        )

        with psycopg2.connect(service='meteo') as mycon:

            df2 = pd.read_sql_query(query_second_plot, mycon)


        if not splot['data']:

            fig2 = go.Figure()

            fig2.add_trace(
                go.Bar(
                    x=df2.month_date,
                    y=df2.no_rain,
                    text='{val} - {where}'.format(
                        val=df2.no_rain.values[0],
                        where=station_dict[station]['it']
                    ),
                    textposition='auto',
                    name='{} - NR'.format(station_dict[station]['it']),
                    marker_color='#f1810c'
                )
            )

            fig2.add_trace(
                go.Bar(
                    x=df2.month_date,
                    y=df2.rain,
                    text='{val} - {where}'.format(
                        val=df2.rain.values[0],
                        where=station_dict[station]['it']
                    ),
                    textposition='auto',
                    name='{} - R'.format(station_dict[station]['it']),
                    marker_color='#209ae8'
                )
            )

            fig2.update_layout(
                title='Not Rainy (orange) VS Rainy (blue) days',
                xaxis_tickformat='%m-%Y',
                bargroupgap=0.1
            )

        else:

            fig2 = go.Figure()

            for i in splot['data']:
                fig2.add_traces(
                    go.Bar(
                        x=i['x'],
                        y=i['y'],
                        text=i['text'],
                        textposition=i['textposition'],
                        marker_color=i['marker']['color'],
                        name=i['name']
                    )
                )

            fig2.add_trace(
                go.Bar(
                    x=df2.month_date,
                    y=df2.no_rain,
                    text='{val} - {where}'.format(
                        val=df2.no_rain.values[0],
                        where=station_dict[station]['it']
                    ),
                    textposition='auto',
                    name='{} - NR'.format(station_dict[station]['it']),
                    marker_color='#f1810c'
                )
            )

            fig2.add_trace(
                go.Bar(
                    x=df2.month_date,
                    y=df2.rain,
                    text='{val} - {where}'.format(
                        val=df2.rain.values[0],
                        where=station_dict[station]['it']
                    ),
                    textposition='auto',
                    name='{} - R'.format(station_dict[station]['it']),
                    marker_color='#209ae8'
                )
            )

            fig2.update_layout(
                title='Not Rainy (orange) VS Rainy (blue) days',
                xaxis_tickformat='%m-%Y',
                bargroupgap=0.1
            )

    except Exception as e:

        fig2 = go.Figure()


    return {'p1': fig1, 'p2': fig2}


# Loading screen CSS
# app.css.append_css({"external_url": "https://codepen.io/chriddyp/pen/brPBPO.css"})

#server = app.server


if __name__ == '__main__':
    app.run_server(debug=True)
