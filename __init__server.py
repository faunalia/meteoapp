import flask
import dash
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output

import plotly.graph_objs as go
import plotly.express as px

import psycopg2
from collections import defaultdict
import requests
import json
import datetime
from datetime import datetime as dt
import pandas as pd



server = flask.Flask(__name__)


station_dict = {'Brunico': {'LT': {'it': 'Temperatura dell´aria', 'de': 'Lufttemperatur', 'unit': '°C'}, 'N': {'it': 'Precipitazioni', 'de': 'Niederschlag', 'unit': 'mm'}, 'WR': {'it': 'Direzione del vento', 'de': 'Windrichtung', 'unit': '° '}, 'WG': {'it': 'Velocità del vento', 'de': 'Windgeschwindigkeit', 'unit': 'm/s'}, 'WG.BOE': {'it': 'Velocitá raffica', 'de': 'Windgeschwindigkeit Böe', 'unit': 'm/s'}, 'LF': {'it': 'Umidità relativa', 'de': 'relative Luftfeuchte', 'unit': '%'}, 'LD.RED': {'it': 'Pressione atmosferica', 'de': 'Luftdruck', 'unit': 'hPa'}, 'GS': {'it': 'Radiazione globale ', 'de': 'Globalstrahlung', 'unit': 'W/m²'}, 'SD': {'it': 'Durata soleggiamento', 'de': 'Sonnenscheindauer', 'unit': 's'}}, 'Bressanone': {'LT': {'it': 'Temperatura dell´aria', 'de': 'Lufttemperatur', 'unit': '°C'}, 'N': {'it': 'Precipitazioni', 'de': 'Niederschlag', 'unit': 'mm'}, 'WR': {'it': 'Direzione del vento', 'de': 'Windrichtung', 'unit': '° '}, 'WG': {'it': 'Velocità del vento', 'de': 'Windgeschwindigkeit', 'unit': 'm/s'}, 'WG.BOE': {'it': 'Velocitá raffica', 'de': 'Windgeschwindigkeit Böe', 'unit': 'm/s'}, 'LF': {'it': 'Umidità relativa', 'de': 'relative Luftfeuchte', 'unit': '%'}, 'LD.RED': {'it': 'Pressione atmosferica', 'de': 'Luftdruck', 'unit': 'hPa'}, 'GS': {'it': 'Radiazione globale ', 'de': 'Globalstrahlung', 'unit': 'W/m²'}, 'SD': {'it': 'Durata soleggiamento', 'de': 'Sonnenscheindauer', 'unit': 's'}}, 'Bolzano': {'LT': {'it': 'Temperatura dell´aria', 'de': 'Lufttemperatur', 'unit': '°C'}, 'N': {'it': 'Precipitazioni', 'de': 'Niederschlag', 'unit': 'mm'}, 'WR': {'it': 'Direzione del vento', 'de': 'Windrichtung', 'unit': '° '}, 'WG': {'it': 'Velocità del vento', 'de': 'Windgeschwindigkeit', 'unit': 'm/s'}, 'WG.BOE': {'it': 'Velocitá raffica', 'de': 'Windgeschwindigkeit Böe', 'unit': 'm/s'}, 'LF': {'it': 'Umidità relativa', 'de': 'relative Luftfeuchte', 'unit': '%'}, 'LD.RED': {'it': 'Pressione atmosferica', 'de': 'Luftdruck', 'unit': 'hPa'}, 'GS': {'it': 'Radiazione globale ', 'de': 'Globalstrahlung', 'unit': 'W/m²'}, 'SD': {'it': 'Durata soleggiamento', 'de': 'Sonnenscheindauer', 'unit': 's'}}, 'Merano': {'LT': {'it': 'Temperatura dell´aria', 'de': 'Lufttemperatur', 'unit': '°C'}, 'N': {'it': 'Precipitazioni', 'de': 'Niederschlag', 'unit': 'mm'}, 'WR': {'it': 'Direzione del vento', 'de': 'Windrichtung', 'unit': '° '}, 'WG': {'it': 'Velocità del vento', 'de': 'Windgeschwindigkeit', 'unit': 'm/s'}, 'WG.BOE': {'it': 'Velocitá raffica', 'de': 'Windgeschwindigkeit Böe', 'unit': 'm/s'}, 'LF': {'it': 'Umidità relativa', 'de': 'relative Luftfeuchte', 'unit': '%'}, 'LD.RED': {'it': 'Pressione atmosferica', 'de': 'Luftdruck', 'unit': 'hPa'}, 'GS': {'it': 'Radiazione globale ', 'de': 'Globalstrahlung', 'unit': 'W/m²'}, 'SD': {'it': 'Durata soleggiamento', 'de': 'Sonnenscheindauer', 'unit': 's'}}}




external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css', dbc.themes.GRID]


# create the Dash app at a specific route
app = dash.Dash(
    __name__,
    server=server,
    routes_pathname_prefix='/meteo-app/',
    external_stylesheets=external_stylesheets
)

# create the Dash App Layout
app.layout = html.Div(
    [
        # header
        html.Div(
            [
                # title and subtitle
                html.Div(
                    [
                        html.A(
                            html.H4(
                                "SouthTyrol Meteo App",
                            ),
                            href="https://www.faunalia.eu",
                            target="_blank",
                            className="app__header__title"
                        ),
                        html.P(
                            "This APP creates Meteo plots of South Tyrol",
                            className="app__header__title--grey"
                        )
                    ],
                className="app__header__desc",
                ),

                # logo
                html.Div(
                    [
                        html.A(
                            html.Img(
                            src=app.get_asset_url("logo_faunalia.png"),
                            className="app__menu__img"
                            ),
                            href="http://www.faunalia.eu",
                            target="_blank"
                        )
                    ],
                    className="app__header__logo",
                ),
            ],
            className="app__header",
        # end of header
        ),

        # entire body that contains all the sandbox (combobox, plots, etc)
        html.Div(
            [

                dbc.Container(
                    [
                        dbc.Row(
                            [
                                # first column
                                dbc.Col(
                                    [
                                        html.Label(
                                            "Select the station"
                                        ),
                                        dcc.Dropdown(
                                            id="stations",
                                            options=[
                                                {"label": k, "value": k} for k, v in station_dict.items()
                                            ],
                                            value='Brunico',
                                            className="auto__p__combo"
                                        )
                                    ]
                                ), # end of first column

                                # second column
                                dbc.Col(
                                    [
                                        html.Label(
                                            "Choose the sensor"
                                        ),
                                        dcc.Dropdown(
                                            id="sensors",
                                            className="auto__p__combo"
                                        )
                                    ]
                                ), # end of second column

                                # third column
                                dbc.Col(
                                    [
                                        html.Label(
                                            "Select the date range"
                                        ),
                                        dcc.DatePickerRange(
                                            id="date-picker-select",
                                            min_date_allowed=dt(2019, 6, 1),
                                            max_date_allowed=dt(2019, 8, 1),
                                            initial_visible_month=dt(2019, 6, 1),
                                            className="auto__p__combo"
                                        )
                                    ]
                                ), # end of third column
                            ]
                        ) # end of row
                    ]
                ), # end of container

                # plots Div
                html.Div(
                    [
                        # left side (main) plot
                        html.Div(
                            [
                                dcc.Graph(
                                    id="main-plot",
                                    # figure=go.Figure()
                                )
                            ],
                        className="sensor__plots"
                        ),

                        # right side plot
                        html.Div(
                            [
                                dcc.Graph(
                                id="secondary-plot",
                                figure=go.Figure()
                            )
                        ],
                        className="sensor__plots"
                        )
                    ],
                    className="app__content",

                )
            ],
        ),

        # footer
        html.Div(
            [
                html.Div(
                    [
                        html.A(
                            html.P(
                                "Original Data taken from the Official South Tyrol Open Data Portal",
                            ),
                            href="http://meteo.provincia.bz.it/",
                            target="_blank",
                            className="app__comment"
                        ),
                    ]
                )
            ]
        )

    ],
    className="app__container",
)


# callback to update the sensor list depending on the station chosen
@app.callback(
    Output('sensors', 'options'),
    [Input('stations', 'value')])
def set_sensors(selected_station):
    return [{'label': v['it'], 'value': k} for k, v in station_dict[selected_station].items()]



# callback to update the main plot
# the only output is the main plot Figure object while different inputs have to
# be specified. If not all the inputs are specified the function returns an empty plot
@app.callback(
    [Output('main-plot', 'figure'),
    Output('secondary-plot', 'figure')],
    [Input('stations', 'value'),
    Input('sensors', 'value'),
    Input('date-picker-select', 'start_date'),
    Input('date-picker-select', 'end_date')]
)
def update_main_plot(station, sensor, start_date, end_date):

    # only if all the input have be defined, call the function to create the Figure
    if station and sensor and start_date and end_date:

        figures = create_plots(station, sensor, start_date, end_date)
        fig1 = figures['p1']
        fig2 = figures['p2']

    # if some inputs are missing, create a valid but empty Figure
    else:
        fig1 = go.Figure()
        fig2 = go.Figure()

    # return the Figure object and show it in the plot
    return fig1, fig2


def create_plots(station, sensor, start_date, end_date):
    '''
    Custom function to be called within the main plot callback

    Returns: dictionary with the 2 Figure objects suitable for the plots

    '''

    ## First Main Plot

    # if the sensor is rain make an hourly SUM of the values
    if sensor == 'N':

        query_main_plot = '''
            SELECT DATE_TRUNC('hour', date) AS dateh, SUM({sensor}) AS rain FROM {station}
                WHERE date BETWEEN '{start_date}' AND '{end_date}'
                AND
                {sensor} IS NOT NULL
                GROUP BY dateh
                ORDER BY dateh DESC
        '''.format(
            sensor=sensor.replace('.', '_').lower(),
            station=station.title(),
            start_date=start_date,
            end_date=end_date
        )

        with psycopg2.connect(service='meteo') as mycon:

            df = pd.read_sql_query(query_main_plot, mycon)

        fig1 = px.bar(
            df,
            x='dateh',
            y='rain',
            labels = {
                'dateh': '',
                'rain' : station_dict[station][sensor]['unit']
            },
            title='mm di pioggia/ora'
        )

        fig1.update_traces(
            name='{}'.format(station),
            showlegend=True,
            marker_color='#40a1d1'
        )

    # other sensors
    else:

        if sensor == 'WR':

            query_main_plot = '''
                SELECT
                    AVG(t) AS wg,
                    wr
                FROM
                    (SELECT
                    --DATE_TRUNC('hour', date) AS dateh,
                    CASE WHEN AVG({sensor}) BETWEEN 45 AND 135 THEN 'E'
                    WHEN AVG({sensor}) BETWEEN 135 AND 225 THEN 'S'
                    WHEN AVG({sensor}) BETWEEN 225 AND 315 THEN 'W'
                    ELSE 'N'
                    END
                    AS wr,
                    AVG(wg) AS t FROM {station}
                    WHERE date BETWEEN '{start_date}' AND '{end_date}'
                    AND
                    {sensor} IS NOT NULL
                    GROUP BY wr) p
                    GROUP BY wr
                    --ORDER BY dateh DESC
            '''.format(
                sensor=sensor.replace('.', '_').lower(),
                station=station.title(),
                start_date=start_date,
                end_date=end_date
            )

            # fetch the data from the query
            # try - except block needed when changing station that does not have sensor
            with psycopg2.connect(service='meteo') as mycon:

                df = pd.read_sql_query(query_main_plot, mycon)

            print(df)

            # first plot
            fig1 = px.bar_polar(
                df,
                r="wg",
                theta="wr",
            )

        else:

            query_main_plot = '''
                SELECT DATE_TRUNC('hour', date) AS dateh, AVG({sensor}) AS {sensor} FROM {station}
                    WHERE date BETWEEN '{start_date}' AND '{end_date}'
                    AND
                    {sensor} IS NOT NULL
                    GROUP BY dateh
                    ORDER BY dateh DESC
            '''.format(
                sensor=sensor.replace('.', '_').lower(),
                station=station.title(),
                start_date=start_date,
                end_date=end_date
            )


            with psycopg2.connect(service='meteo') as mycon:

                df = pd.read_sql_query(query_main_plot, mycon)


            fig1 = px.line(
                df,
                x='dateh',
                y=sensor.replace('.', '_').lower(),
                labels = {
                    'dateh': '',
                    sensor.replace('.', '_').lower() : station_dict[station][sensor]['unit']
                },
                title='{} a {}'.format(
                    station_dict[station][sensor]['it'],
                    station)
            )

            fig1.update_traces(
                name='{}'.format(station),
                showlegend=True,
                line_color='#1262db'
            )


    ## Second Plot

    # try - except mandatory if the second station chosen does not have the N sensor
    try:

        query_second_plot = '''
            SELECT
                CONCAT(EXTRACT(month FROM day_date), '-', EXTRACT(year FROM day_date)) AS mese,
                DATE_TRUNC('month', day_date) AS month_date,
                COUNT(n) FILTER (WHERE n = 0) AS no_rain,
                COUNT(n) FILTER (WHERE n != 0) AS rain,
                COUNT(n) AS all_values
                FROM
                    (SELECT
                    DATE_TRUNC('day', date) AS day_date,
                    SUM(n) AS n FROM {station}
                    WHERE date BETWEEN '{start_date}' AND '{end_date}'
                    GROUP BY day_date
                    ORDER BY day_date desc) p
                GROUP BY mese, month_date
                ORDER BY month_date DESC
        '''.format(
            station=station.title(),
            start_date=start_date,
            end_date=end_date
        )

        print(query_second_plot)

        with psycopg2.connect(service='meteo') as mycon:

            df = pd.read_sql_query(query_second_plot, mycon)

        data2 = [
            go.Bar(
                x=df.mese,
                y=df.no_rain,
                text=df.no_rain,
                textposition='auto',
                name='No Rainy days',
                marker_color='rgb(241, 129, 12)'
            ),
            go.Bar(
                x=df.mese,
                y=df.rain,
                text=df.rain,
                textposition='auto',
                name='Rainy days',
                marker_color='rgb(32, 154, 232)'
            ),
        ]

        layout2 = go.Layout(
            title='Giorni di pioggia',
            # xaxis=dict(
            #     tickformat='%m-%Y'
            # ),
            yaxis=dict(
                title='Numero di giorni',
            ),
        )
        fig2 = go.Figure(data=data2, layout=layout2)

    except Exception as e:

        fig2 = go.Figure()

    return {'p1': fig1, 'p2': fig2}





#server = app.server


if __name__ == '__main__':
    app.run_server(debug=True)
